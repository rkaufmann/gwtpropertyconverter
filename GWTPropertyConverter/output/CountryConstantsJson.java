package com.roksoft.pocoso3.client.resources;
import java.util.Map;
import java.util.LinkedHashMap;
import com.google.gwt.json.client.JSONObject;
public class CountryConstantsJson extends JSONObject implements CountryConstants{ 
public final native String getVU() /*-{	return $wnd.CountryConstants.VU;}-*/;
@Override
public String VU(){return getVU();}
public final native String getVN() /*-{	return $wnd.CountryConstants.VN;}-*/;
@Override
public String VN(){return getVN();}
public final native String getEC() /*-{	return $wnd.CountryConstants.EC;}-*/;
@Override
public String EC(){return getEC();}
public final native String getVI() /*-{	return $wnd.CountryConstants.VI;}-*/;
@Override
public String VI(){return getVI();}
public final native String getDZ() /*-{	return $wnd.CountryConstants.DZ;}-*/;
@Override
public String DZ(){return getDZ();}
public final native String getVG() /*-{	return $wnd.CountryConstants.VG;}-*/;
@Override
public String VG(){return getVG();}
public final native String getVE() /*-{	return $wnd.CountryConstants.VE;}-*/;
@Override
public String VE(){return getVE();}
public final native String getDM() /*-{	return $wnd.CountryConstants.DM;}-*/;
@Override
public String DM(){return getDM();}
public final native String getVC() /*-{	return $wnd.CountryConstants.VC;}-*/;
@Override
public String VC(){return getVC();}
public final native String getDO() /*-{	return $wnd.CountryConstants.DO;}-*/;
@Override
public String DO(){return getDO();}
public final native String getVA() /*-{	return $wnd.CountryConstants.VA;}-*/;
@Override
public String VA(){return getVA();}
public final native String getDE() /*-{	return $wnd.CountryConstants.DE;}-*/;
@Override
public String DE(){return getDE();}
public final native String getUZ() /*-{	return $wnd.CountryConstants.UZ;}-*/;
@Override
public String UZ(){return getUZ();}
public final native String getUY() /*-{	return $wnd.CountryConstants.UY;}-*/;
@Override
public String UY(){return getUY();}
public final native String getDK() /*-{	return $wnd.CountryConstants.DK;}-*/;
@Override
public String DK(){return getDK();}
public final native String getDJ() /*-{	return $wnd.CountryConstants.DJ;}-*/;
@Override
public String DJ(){return getDJ();}
public final native String getUS() /*-{	return $wnd.CountryConstants.US;}-*/;
@Override
public String US(){return getUS();}
public final native String getUM() /*-{	return $wnd.CountryConstants.UM;}-*/;
@Override
public String UM(){return getUM();}
public final native String getUG() /*-{	return $wnd.CountryConstants.UG;}-*/;
@Override
public String UG(){return getUG();}
public final native String getUA() /*-{	return $wnd.CountryConstants.UA;}-*/;
@Override
public String UA(){return getUA();}
public final native String getET() /*-{	return $wnd.CountryConstants.ET;}-*/;
@Override
public String ET(){return getET();}
public final native String getES() /*-{	return $wnd.CountryConstants.ES;}-*/;
@Override
public String ES(){return getES();}
public final native String getER() /*-{	return $wnd.CountryConstants.ER;}-*/;
@Override
public String ER(){return getER();}
public final native String getEH() /*-{	return $wnd.CountryConstants.EH;}-*/;
@Override
public String EH(){return getEH();}
public final native String getEG() /*-{	return $wnd.CountryConstants.EG;}-*/;
@Override
public String EG(){return getEG();}
public final native String getTZ() /*-{	return $wnd.CountryConstants.TZ;}-*/;
@Override
public String TZ(){return getTZ();}
public final native String getEE() /*-{	return $wnd.CountryConstants.EE;}-*/;
@Override
public String EE(){return getEE();}
public final native String getTT() /*-{	return $wnd.CountryConstants.TT;}-*/;
@Override
public String TT(){return getTT();}
public final native String getTW() /*-{	return $wnd.CountryConstants.TW;}-*/;
@Override
public String TW(){return getTW();}
public final native String getTV() /*-{	return $wnd.CountryConstants.TV;}-*/;
@Override
public String TV(){return getTV();}
public final native String getGD() /*-{	return $wnd.CountryConstants.GD;}-*/;
@Override
public String GD(){return getGD();}
public final native String getGE() /*-{	return $wnd.CountryConstants.GE;}-*/;
@Override
public String GE(){return getGE();}
public final native String getGF() /*-{	return $wnd.CountryConstants.GF;}-*/;
@Override
public String GF(){return getGF();}
public final native String getGA() /*-{	return $wnd.CountryConstants.GA;}-*/;
@Override
public String GA(){return getGA();}
public final native String getGB() /*-{	return $wnd.CountryConstants.GB;}-*/;
@Override
public String GB(){return getGB();}
public final native String getFR() /*-{	return $wnd.CountryConstants.FR;}-*/;
@Override
public String FR(){return getFR();}
public final native String getFO() /*-{	return $wnd.CountryConstants.FO;}-*/;
@Override
public String FO(){return getFO();}
public final native String getFK() /*-{	return $wnd.CountryConstants.FK;}-*/;
@Override
public String FK(){return getFK();}
public final native String getFJ() /*-{	return $wnd.CountryConstants.FJ;}-*/;
@Override
public String FJ(){return getFJ();}
public final native String getFM() /*-{	return $wnd.CountryConstants.FM;}-*/;
@Override
public String FM(){return getFM();}
public final native String getFI() /*-{	return $wnd.CountryConstants.FI;}-*/;
@Override
public String FI(){return getFI();}
public final native String getWS() /*-{	return $wnd.CountryConstants.WS;}-*/;
@Override
public String WS(){return getWS();}
public final native String getGY() /*-{	return $wnd.CountryConstants.GY;}-*/;
@Override
public String GY(){return getGY();}
public final native String getGW() /*-{	return $wnd.CountryConstants.GW;}-*/;
@Override
public String GW(){return getGW();}
public final native String getGU() /*-{	return $wnd.CountryConstants.GU;}-*/;
@Override
public String GU(){return getGU();}
public final native String getGT() /*-{	return $wnd.CountryConstants.GT;}-*/;
@Override
public String GT(){return getGT();}
public final native String getGS() /*-{	return $wnd.CountryConstants.GS;}-*/;
@Override
public String GS(){return getGS();}
public final native String getGR() /*-{	return $wnd.CountryConstants.GR;}-*/;
@Override
public String GR(){return getGR();}
public final native String getGQ() /*-{	return $wnd.CountryConstants.GQ;}-*/;
@Override
public String GQ(){return getGQ();}
public final native String getWF() /*-{	return $wnd.CountryConstants.WF;}-*/;
@Override
public String WF(){return getWF();}
public final native String getGP() /*-{	return $wnd.CountryConstants.GP;}-*/;
@Override
public String GP(){return getGP();}
public final native String getGN() /*-{	return $wnd.CountryConstants.GN;}-*/;
@Override
public String GN(){return getGN();}
public final native String getGM() /*-{	return $wnd.CountryConstants.GM;}-*/;
@Override
public String GM(){return getGM();}
public final native String getGL() /*-{	return $wnd.CountryConstants.GL;}-*/;
@Override
public String GL(){return getGL();}
public final native String getGI() /*-{	return $wnd.CountryConstants.GI;}-*/;
@Override
public String GI(){return getGI();}
public final native String getGH() /*-{	return $wnd.CountryConstants.GH;}-*/;
@Override
public String GH(){return getGH();}
public final native String getGG() /*-{	return $wnd.CountryConstants.GG;}-*/;
@Override
public String GG(){return getGG();}
public final native String getRE() /*-{	return $wnd.CountryConstants.RE;}-*/;
@Override
public String RE(){return getRE();}
public final native String getRO() /*-{	return $wnd.CountryConstants.RO;}-*/;
@Override
public String RO(){return getRO();}
public final native String getAT() /*-{	return $wnd.CountryConstants.AT;}-*/;
@Override
public String AT(){return getAT();}
public final native String getAS() /*-{	return $wnd.CountryConstants.AS;}-*/;
@Override
public String AS(){return getAS();}
public final native String getAR() /*-{	return $wnd.CountryConstants.AR;}-*/;
@Override
public String AR(){return getAR();}
public final native String getAQ() /*-{	return $wnd.CountryConstants.AQ;}-*/;
@Override
public String AQ(){return getAQ();}
public final native String getAX() /*-{	return $wnd.CountryConstants.AX;}-*/;
@Override
public String AX(){return getAX();}
public final native String getQA() /*-{	return $wnd.CountryConstants.QA;}-*/;
@Override
public String QA(){return getQA();}
public final native String getAW() /*-{	return $wnd.CountryConstants.AW;}-*/;
@Override
public String AW(){return getAW();}
public final native String getAU() /*-{	return $wnd.CountryConstants.AU;}-*/;
@Override
public String AU(){return getAU();}
public final native String getAZ() /*-{	return $wnd.CountryConstants.AZ;}-*/;
@Override
public String AZ(){return getAZ();}
public final native String getBA() /*-{	return $wnd.CountryConstants.BA;}-*/;
@Override
public String BA(){return getBA();}
public final native String getPT() /*-{	return $wnd.CountryConstants.PT;}-*/;
@Override
public String PT(){return getPT();}
public final native String getAD() /*-{	return $wnd.CountryConstants.AD;}-*/;
@Override
public String AD(){return getAD();}
public final native String getPW() /*-{	return $wnd.CountryConstants.PW;}-*/;
@Override
public String PW(){return getPW();}
public final native String getAG() /*-{	return $wnd.CountryConstants.AG;}-*/;
@Override
public String AG(){return getAG();}
public final native String getPR() /*-{	return $wnd.CountryConstants.PR;}-*/;
@Override
public String PR(){return getPR();}
public final native String getAE() /*-{	return $wnd.CountryConstants.AE;}-*/;
@Override
public String AE(){return getAE();}
public final native String getPS() /*-{	return $wnd.CountryConstants.PS;}-*/;
@Override
public String PS(){return getPS();}
public final native String getAF() /*-{	return $wnd.CountryConstants.AF;}-*/;
@Override
public String AF(){return getAF();}
public final native String getAL() /*-{	return $wnd.CountryConstants.AL;}-*/;
@Override
public String AL(){return getAL();}
public final native String getAI() /*-{	return $wnd.CountryConstants.AI;}-*/;
@Override
public String AI(){return getAI();}
public final native String getAO() /*-{	return $wnd.CountryConstants.AO;}-*/;
@Override
public String AO(){return getAO();}
public final native String getPY() /*-{	return $wnd.CountryConstants.PY;}-*/;
@Override
public String PY(){return getPY();}
public final native String getAM() /*-{	return $wnd.CountryConstants.AM;}-*/;
@Override
public String AM(){return getAM();}
public final native String getAN() /*-{	return $wnd.CountryConstants.AN;}-*/;
@Override
public String AN(){return getAN();}
public final native String getTG() /*-{	return $wnd.CountryConstants.TG;}-*/;
@Override
public String TG(){return getTG();}
public final native String getBW() /*-{	return $wnd.CountryConstants.BW;}-*/;
@Override
public String BW(){return getBW();}
public final native String getTF() /*-{	return $wnd.CountryConstants.TF;}-*/;
@Override
public String TF(){return getTF();}
public final native String getBV() /*-{	return $wnd.CountryConstants.BV;}-*/;
@Override
public String BV(){return getBV();}
public final native String getBY() /*-{	return $wnd.CountryConstants.BY;}-*/;
@Override
public String BY(){return getBY();}
public final native String getTD() /*-{	return $wnd.CountryConstants.TD;}-*/;
@Override
public String TD(){return getTD();}
public final native String getTK() /*-{	return $wnd.CountryConstants.TK;}-*/;
@Override
public String TK(){return getTK();}
public final native String getBS() /*-{	return $wnd.CountryConstants.BS;}-*/;
@Override
public String BS(){return getBS();}
public final native String getTJ() /*-{	return $wnd.CountryConstants.TJ;}-*/;
@Override
public String TJ(){return getTJ();}
public final native String getBR() /*-{	return $wnd.CountryConstants.BR;}-*/;
@Override
public String BR(){return getBR();}
public final native String getTH() /*-{	return $wnd.CountryConstants.TH;}-*/;
@Override
public String TH(){return getTH();}
public final native String getBT() /*-{	return $wnd.CountryConstants.BT;}-*/;
@Override
public String BT(){return getBT();}
public final native String getTO() /*-{	return $wnd.CountryConstants.TO;}-*/;
@Override
public String TO(){return getTO();}
public final native String getTN() /*-{	return $wnd.CountryConstants.TN;}-*/;
@Override
public String TN(){return getTN();}
public final native String getTM() /*-{	return $wnd.CountryConstants.TM;}-*/;
@Override
public String TM(){return getTM();}
public final native String getTL() /*-{	return $wnd.CountryConstants.TL;}-*/;
@Override
public String TL(){return getTL();}
public final native String getCA() /*-{	return $wnd.CountryConstants.CA;}-*/;
@Override
public String CA(){return getCA();}
public final native String getTR() /*-{	return $wnd.CountryConstants.TR;}-*/;
@Override
public String TR(){return getTR();}
public final native String getBZ() /*-{	return $wnd.CountryConstants.BZ;}-*/;
@Override
public String BZ(){return getBZ();}
public final native String getBF() /*-{	return $wnd.CountryConstants.BF;}-*/;
@Override
public String BF(){return getBF();}
public final native String getSV() /*-{	return $wnd.CountryConstants.SV;}-*/;
@Override
public String SV(){return getSV();}
public final native String getBG() /*-{	return $wnd.CountryConstants.BG;}-*/;
@Override
public String BG(){return getBG();}
public final native String getBH() /*-{	return $wnd.CountryConstants.BH;}-*/;
@Override
public String BH(){return getBH();}
public final native String getST() /*-{	return $wnd.CountryConstants.ST;}-*/;
@Override
public String ST(){return getST();}
public final native String getBI() /*-{	return $wnd.CountryConstants.BI;}-*/;
@Override
public String BI(){return getBI();}
public final native String getSY() /*-{	return $wnd.CountryConstants.SY;}-*/;
@Override
public String SY(){return getSY();}
public final native String getBB() /*-{	return $wnd.CountryConstants.BB;}-*/;
@Override
public String BB(){return getBB();}
public final native String getSZ() /*-{	return $wnd.CountryConstants.SZ;}-*/;
@Override
public String SZ(){return getSZ();}
public final native String getBD() /*-{	return $wnd.CountryConstants.BD;}-*/;
@Override
public String BD(){return getBD();}
public final native String getBE() /*-{	return $wnd.CountryConstants.BE;}-*/;
@Override
public String BE(){return getBE();}
public final native String getBN() /*-{	return $wnd.CountryConstants.BN;}-*/;
@Override
public String BN(){return getBN();}
public final native String getBO() /*-{	return $wnd.CountryConstants.BO;}-*/;
@Override
public String BO(){return getBO();}
public final native String getBJ() /*-{	return $wnd.CountryConstants.BJ;}-*/;
@Override
public String BJ(){return getBJ();}
public final native String getTC() /*-{	return $wnd.CountryConstants.TC;}-*/;
@Override
public String TC(){return getTC();}
public final native String getBL() /*-{	return $wnd.CountryConstants.BL;}-*/;
@Override
public String BL(){return getBL();}
public final native String getBM() /*-{	return $wnd.CountryConstants.BM;}-*/;
@Override
public String BM(){return getBM();}
public final native String getCZ() /*-{	return $wnd.CountryConstants.CZ;}-*/;
@Override
public String CZ(){return getCZ();}
public final native String getSD() /*-{	return $wnd.CountryConstants.SD;}-*/;
@Override
public String SD(){return getSD();}
public final native String getCY() /*-{	return $wnd.CountryConstants.CY;}-*/;
@Override
public String CY(){return getCY();}
public final native String getSC() /*-{	return $wnd.CountryConstants.SC;}-*/;
@Override
public String SC(){return getSC();}
public final native String getCX() /*-{	return $wnd.CountryConstants.CX;}-*/;
@Override
public String CX(){return getCX();}
public final native String getSE() /*-{	return $wnd.CountryConstants.SE;}-*/;
@Override
public String SE(){return getSE();}
public final native String getCV() /*-{	return $wnd.CountryConstants.CV;}-*/;
@Override
public String CV(){return getCV();}
public final native String getSH() /*-{	return $wnd.CountryConstants.SH;}-*/;
@Override
public String SH(){return getSH();}
public final native String getCU() /*-{	return $wnd.CountryConstants.CU;}-*/;
@Override
public String CU(){return getCU();}
public final native String getSG() /*-{	return $wnd.CountryConstants.SG;}-*/;
@Override
public String SG(){return getSG();}
public final native String getSJ() /*-{	return $wnd.CountryConstants.SJ;}-*/;
@Override
public String SJ(){return getSJ();}
public final native String getCS() /*-{	return $wnd.CountryConstants.CS;}-*/;
@Override
public String CS(){return getCS();}
public final native String getSI() /*-{	return $wnd.CountryConstants.SI;}-*/;
@Override
public String SI(){return getSI();}
public final native String getSL() /*-{	return $wnd.CountryConstants.SL;}-*/;
@Override
public String SL(){return getSL();}
public final native String getSK() /*-{	return $wnd.CountryConstants.SK;}-*/;
@Override
public String SK(){return getSK();}
public final native String getSN() /*-{	return $wnd.CountryConstants.SN;}-*/;
@Override
public String SN(){return getSN();}
public final native String getSM() /*-{	return $wnd.CountryConstants.SM;}-*/;
@Override
public String SM(){return getSM();}
public final native String getSO() /*-{	return $wnd.CountryConstants.SO;}-*/;
@Override
public String SO(){return getSO();}
public final native String getSR() /*-{	return $wnd.CountryConstants.SR;}-*/;
@Override
public String SR(){return getSR();}
public final native String getCI() /*-{	return $wnd.CountryConstants.CI;}-*/;
@Override
public String CI(){return getCI();}
public final native String getRS() /*-{	return $wnd.CountryConstants.RS;}-*/;
@Override
public String RS(){return getRS();}
public final native String getCG() /*-{	return $wnd.CountryConstants.CG;}-*/;
@Override
public String CG(){return getCG();}
public final native String getRU() /*-{	return $wnd.CountryConstants.RU;}-*/;
@Override
public String RU(){return getRU();}
public final native String getCH() /*-{	return $wnd.CountryConstants.CH;}-*/;
@Override
public String CH(){return getCH();}
public final native String getRW() /*-{	return $wnd.CountryConstants.RW;}-*/;
@Override
public String RW(){return getRW();}
public final native String getCF() /*-{	return $wnd.CountryConstants.CF;}-*/;
@Override
public String CF(){return getCF();}
public final native String getCC() /*-{	return $wnd.CountryConstants.CC;}-*/;
@Override
public String CC(){return getCC();}
public final native String getCD() /*-{	return $wnd.CountryConstants.CD;}-*/;
@Override
public String CD(){return getCD();}
public final native String getCR() /*-{	return $wnd.CountryConstants.CR;}-*/;
@Override
public String CR(){return getCR();}
public final native String getCO() /*-{	return $wnd.CountryConstants.CO;}-*/;
@Override
public String CO(){return getCO();}
public final native String getCM() /*-{	return $wnd.CountryConstants.CM;}-*/;
@Override
public String CM(){return getCM();}
public final native String getCN() /*-{	return $wnd.CountryConstants.CN;}-*/;
@Override
public String CN(){return getCN();}
public final native String getSA() /*-{	return $wnd.CountryConstants.SA;}-*/;
@Override
public String SA(){return getSA();}
public final native String getCK() /*-{	return $wnd.CountryConstants.CK;}-*/;
@Override
public String CK(){return getCK();}
public final native String getSB() /*-{	return $wnd.CountryConstants.SB;}-*/;
@Override
public String SB(){return getSB();}
public final native String getCL() /*-{	return $wnd.CountryConstants.CL;}-*/;
@Override
public String CL(){return getCL();}
public final native String getLV() /*-{	return $wnd.CountryConstants.LV;}-*/;
@Override
public String LV(){return getLV();}
public final native String getLU() /*-{	return $wnd.CountryConstants.LU;}-*/;
@Override
public String LU(){return getLU();}
public final native String getLT() /*-{	return $wnd.CountryConstants.LT;}-*/;
@Override
public String LT(){return getLT();}
public final native String getLY() /*-{	return $wnd.CountryConstants.LY;}-*/;
@Override
public String LY(){return getLY();}
public final native String getLS() /*-{	return $wnd.CountryConstants.LS;}-*/;
@Override
public String LS(){return getLS();}
public final native String getLR() /*-{	return $wnd.CountryConstants.LR;}-*/;
@Override
public String LR(){return getLR();}
public final native String getMG() /*-{	return $wnd.CountryConstants.MG;}-*/;
@Override
public String MG(){return getMG();}
public final native String getMH() /*-{	return $wnd.CountryConstants.MH;}-*/;
@Override
public String MH(){return getMH();}
public final native String getME() /*-{	return $wnd.CountryConstants.ME;}-*/;
@Override
public String ME(){return getME();}
public final native String getMF() /*-{	return $wnd.CountryConstants.MF;}-*/;
@Override
public String MF(){return getMF();}
public final native String getMK() /*-{	return $wnd.CountryConstants.MK;}-*/;
@Override
public String MK(){return getMK();}
public final native String getML() /*-{	return $wnd.CountryConstants.ML;}-*/;
@Override
public String ML(){return getML();}
Map<String, String> internalCountryMap = null;
public void addCountryMapEntry(String key, String value) {	internalCountryMap.put(key, value);}@Override
public Map<String, String> CountryMap(){if (internalCountryMap==null){
internalCountryMap = new LinkedHashMap<String, String>();
String stringValues = getCountryMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addCountryMapValueForKey(valueArray[i].trim());}
}return internalCountryMap;}
private final native String getCountryMap() /*-{return $wnd.CountryConstants.CountryMap;}-*/;public final native void addCountryMapValueForKey(String key) /*-{
var value = $wnd.CountryConstants[key];
this.@com.roksoft.pocoso3.client.resources.CountryConstantsJson::addCountryMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getMC() /*-{	return $wnd.CountryConstants.MC;}-*/;
@Override
public String MC(){return getMC();}
public final native String getMD() /*-{	return $wnd.CountryConstants.MD;}-*/;
@Override
public String MD(){return getMD();}
public final native String getMA() /*-{	return $wnd.CountryConstants.MA;}-*/;
@Override
public String MA(){return getMA();}
public final native String getMV() /*-{	return $wnd.CountryConstants.MV;}-*/;
@Override
public String MV(){return getMV();}
public final native String getMU() /*-{	return $wnd.CountryConstants.MU;}-*/;
@Override
public String MU(){return getMU();}
public final native String getMX() /*-{	return $wnd.CountryConstants.MX;}-*/;
@Override
public String MX(){return getMX();}
public final native String getMW() /*-{	return $wnd.CountryConstants.MW;}-*/;
@Override
public String MW(){return getMW();}
public final native String getMZ() /*-{	return $wnd.CountryConstants.MZ;}-*/;
@Override
public String MZ(){return getMZ();}
public final native String getMY() /*-{	return $wnd.CountryConstants.MY;}-*/;
@Override
public String MY(){return getMY();}
public final native String getMN() /*-{	return $wnd.CountryConstants.MN;}-*/;
@Override
public String MN(){return getMN();}
public final native String getMM() /*-{	return $wnd.CountryConstants.MM;}-*/;
@Override
public String MM(){return getMM();}
public final native String getMP() /*-{	return $wnd.CountryConstants.MP;}-*/;
@Override
public String MP(){return getMP();}
public final native String getMO() /*-{	return $wnd.CountryConstants.MO;}-*/;
@Override
public String MO(){return getMO();}
public final native String getMR() /*-{	return $wnd.CountryConstants.MR;}-*/;
@Override
public String MR(){return getMR();}
public final native String getMQ() /*-{	return $wnd.CountryConstants.MQ;}-*/;
@Override
public String MQ(){return getMQ();}
public final native String getMT() /*-{	return $wnd.CountryConstants.MT;}-*/;
@Override
public String MT(){return getMT();}
public final native String getMS() /*-{	return $wnd.CountryConstants.MS;}-*/;
@Override
public String MS(){return getMS();}
public final native String getNF() /*-{	return $wnd.CountryConstants.NF;}-*/;
@Override
public String NF(){return getNF();}
public final native String getNG() /*-{	return $wnd.CountryConstants.NG;}-*/;
@Override
public String NG(){return getNG();}
public final native String getNI() /*-{	return $wnd.CountryConstants.NI;}-*/;
@Override
public String NI(){return getNI();}
public final native String getNL() /*-{	return $wnd.CountryConstants.NL;}-*/;
@Override
public String NL(){return getNL();}
public final native String getNA() /*-{	return $wnd.CountryConstants.NA;}-*/;
@Override
public String NA(){return getNA();}
public final native String getNC() /*-{	return $wnd.CountryConstants.NC;}-*/;
@Override
public String NC(){return getNC();}
public final native String getNE() /*-{	return $wnd.CountryConstants.NE;}-*/;
@Override
public String NE(){return getNE();}
public final native String getNZ() /*-{	return $wnd.CountryConstants.NZ;}-*/;
@Override
public String NZ(){return getNZ();}
public final native String getNU() /*-{	return $wnd.CountryConstants.NU;}-*/;
@Override
public String NU(){return getNU();}
public final native String getNR() /*-{	return $wnd.CountryConstants.NR;}-*/;
@Override
public String NR(){return getNR();}
public final native String getNP() /*-{	return $wnd.CountryConstants.NP;}-*/;
@Override
public String NP(){return getNP();}
public final native String getNO() /*-{	return $wnd.CountryConstants.NO;}-*/;
@Override
public String NO(){return getNO();}
public final native String getOM() /*-{	return $wnd.CountryConstants.OM;}-*/;
@Override
public String OM(){return getOM();}
public final native String getPL() /*-{	return $wnd.CountryConstants.PL;}-*/;
@Override
public String PL(){return getPL();}
public final native String getPM() /*-{	return $wnd.CountryConstants.PM;}-*/;
@Override
public String PM(){return getPM();}
public final native String getPN() /*-{	return $wnd.CountryConstants.PN;}-*/;
@Override
public String PN(){return getPN();}
public final native String getPH() /*-{	return $wnd.CountryConstants.PH;}-*/;
@Override
public String PH(){return getPH();}
public final native String getPK() /*-{	return $wnd.CountryConstants.PK;}-*/;
@Override
public String PK(){return getPK();}
public final native String getPE() /*-{	return $wnd.CountryConstants.PE;}-*/;
@Override
public String PE(){return getPE();}
public final native String getPF() /*-{	return $wnd.CountryConstants.PF;}-*/;
@Override
public String PF(){return getPF();}
public final native String getPG() /*-{	return $wnd.CountryConstants.PG;}-*/;
@Override
public String PG(){return getPG();}
public final native String getPA() /*-{	return $wnd.CountryConstants.PA;}-*/;
@Override
public String PA(){return getPA();}
public final native String getHK() /*-{	return $wnd.CountryConstants.HK;}-*/;
@Override
public String HK(){return getHK();}
public final native String getZA() /*-{	return $wnd.CountryConstants.ZA;}-*/;
@Override
public String ZA(){return getZA();}
public final native String getHN() /*-{	return $wnd.CountryConstants.HN;}-*/;
@Override
public String HN(){return getHN();}
public final native String getHM() /*-{	return $wnd.CountryConstants.HM;}-*/;
@Override
public String HM(){return getHM();}
public final native String getHR() /*-{	return $wnd.CountryConstants.HR;}-*/;
@Override
public String HR(){return getHR();}
public final native String getHT() /*-{	return $wnd.CountryConstants.HT;}-*/;
@Override
public String HT(){return getHT();}
public final native String getHU() /*-{	return $wnd.CountryConstants.HU;}-*/;
@Override
public String HU(){return getHU();}
public final native String getZM() /*-{	return $wnd.CountryConstants.ZM;}-*/;
@Override
public String ZM(){return getZM();}
public final native String getID() /*-{	return $wnd.CountryConstants.ID;}-*/;
@Override
public String ID(){return getID();}
public final native String getZW() /*-{	return $wnd.CountryConstants.ZW;}-*/;
@Override
public String ZW(){return getZW();}
public final native String getIE() /*-{	return $wnd.CountryConstants.IE;}-*/;
@Override
public String IE(){return getIE();}
public final native String getIL() /*-{	return $wnd.CountryConstants.IL;}-*/;
@Override
public String IL(){return getIL();}
public final native String getIM() /*-{	return $wnd.CountryConstants.IM;}-*/;
@Override
public String IM(){return getIM();}
public final native String getIN() /*-{	return $wnd.CountryConstants.IN;}-*/;
@Override
public String IN(){return getIN();}
public final native String getIO() /*-{	return $wnd.CountryConstants.IO;}-*/;
@Override
public String IO(){return getIO();}
public final native String getIQ() /*-{	return $wnd.CountryConstants.IQ;}-*/;
@Override
public String IQ(){return getIQ();}
public final native String getIR() /*-{	return $wnd.CountryConstants.IR;}-*/;
@Override
public String IR(){return getIR();}
public final native String getIS() /*-{	return $wnd.CountryConstants.IS;}-*/;
@Override
public String IS(){return getIS();}
public final native String getYE() /*-{	return $wnd.CountryConstants.YE;}-*/;
@Override
public String YE(){return getYE();}
public final native String getIT() /*-{	return $wnd.CountryConstants.IT;}-*/;
@Override
public String IT(){return getIT();}
public final native String getJE() /*-{	return $wnd.CountryConstants.JE;}-*/;
@Override
public String JE(){return getJE();}
public final native String getYT() /*-{	return $wnd.CountryConstants.YT;}-*/;
@Override
public String YT(){return getYT();}
public final native String getJP() /*-{	return $wnd.CountryConstants.JP;}-*/;
@Override
public String JP(){return getJP();}
public final native String getJO() /*-{	return $wnd.CountryConstants.JO;}-*/;
@Override
public String JO(){return getJO();}
public final native String getJM() /*-{	return $wnd.CountryConstants.JM;}-*/;
@Override
public String JM(){return getJM();}
public final native String getKI() /*-{	return $wnd.CountryConstants.KI;}-*/;
@Override
public String KI(){return getKI();}
public final native String getKH() /*-{	return $wnd.CountryConstants.KH;}-*/;
@Override
public String KH(){return getKH();}
public final native String getKG() /*-{	return $wnd.CountryConstants.KG;}-*/;
@Override
public String KG(){return getKG();}
public final native String getKE() /*-{	return $wnd.CountryConstants.KE;}-*/;
@Override
public String KE(){return getKE();}
public final native String getKP() /*-{	return $wnd.CountryConstants.KP;}-*/;
@Override
public String KP(){return getKP();}
public final native String getZZ() /*-{	return $wnd.CountryConstants.ZZ;}-*/;
@Override
public String ZZ(){return getZZ();}
public final native String getKR() /*-{	return $wnd.CountryConstants.KR;}-*/;
@Override
public String KR(){return getKR();}
public final native String getKM() /*-{	return $wnd.CountryConstants.KM;}-*/;
@Override
public String KM(){return getKM();}
public final native String getKN() /*-{	return $wnd.CountryConstants.KN;}-*/;
@Override
public String KN(){return getKN();}
public final native String getKW() /*-{	return $wnd.CountryConstants.KW;}-*/;
@Override
public String KW(){return getKW();}
public final native String getKY() /*-{	return $wnd.CountryConstants.KY;}-*/;
@Override
public String KY(){return getKY();}
public final native String getKZ() /*-{	return $wnd.CountryConstants.KZ;}-*/;
@Override
public String KZ(){return getKZ();}
public final native String getLA() /*-{	return $wnd.CountryConstants.LA;}-*/;
@Override
public String LA(){return getLA();}
public final native String getLC() /*-{	return $wnd.CountryConstants.LC;}-*/;
@Override
public String LC(){return getLC();}
public final native String getLB() /*-{	return $wnd.CountryConstants.LB;}-*/;
@Override
public String LB(){return getLB();}
public final native String getLI() /*-{	return $wnd.CountryConstants.LI;}-*/;
@Override
public String LI(){return getLI();}
public final native String getLK() /*-{	return $wnd.CountryConstants.LK;}-*/;
@Override
public String LK(){return getLK();}

} 