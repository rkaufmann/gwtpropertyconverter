package com.roksoft.pocoso3.client.resources;
import java.util.Map;
import java.util.LinkedHashMap;
import com.google.gwt.json.client.JSONObject;
public class EditorConstantsJson extends JSONObject implements EditorConstants{ 
public final native String getSINGLE_SCORE_POOMSE() /*-{	return $wnd.EditorConstants.SINGLE_SCORE_POOMSE;}-*/;
@Override
public String SINGLE_SCORE_POOMSE(){return getSINGLE_SCORE_POOMSE();}
public final native String getCUT_OFF_TOURNAMENT() /*-{	return $wnd.EditorConstants.CUT_OFF_TOURNAMENT;}-*/;
@Override
public String CUT_OFF_TOURNAMENT(){return getCUT_OFF_TOURNAMENT();}
public final native String getMALE() /*-{	return $wnd.EditorConstants.MALE;}-*/;
@Override
public String MALE(){return getMALE();}
public final native String getRECOGNIZED_POOMSE() /*-{	return $wnd.EditorConstants.RECOGNIZED_POOMSE;}-*/;
@Override
public String RECOGNIZED_POOMSE(){return getRECOGNIZED_POOMSE();}
public final native String getFEMALE() /*-{	return $wnd.EditorConstants.FEMALE;}-*/;
@Override
public String FEMALE(){return getFEMALE();}
Map<String, String> internalSexMap = null;
public void addSexMapEntry(String key, String value) {	internalSexMap.put(key, value);}@Override
public Map<String, String> SexMap(){if (internalSexMap==null){
internalSexMap = new LinkedHashMap<String, String>();
String stringValues = getSexMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addSexMapValueForKey(valueArray[i].trim());}
}return internalSexMap;}
private final native String getSexMap() /*-{return $wnd.EditorConstants.SexMap;}-*/;public final native void addSexMapValueForKey(String key) /*-{
var value = $wnd.EditorConstants[key];
this.@com.roksoft.pocoso3.client.resources.EditorConstantsJson::addSexMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getLabelResults() /*-{	return $wnd.EditorConstants.LabelResults;}-*/;
@Override
public String LabelResults(){return getLabelResults();}
public final native String getLabelLoad() /*-{	return $wnd.EditorConstants.LabelLoad;}-*/;
@Override
public String LabelLoad(){return getLabelLoad();}
public final native String getFREESTYLE_POOMSE() /*-{	return $wnd.EditorConstants.FREESTYLE_POOMSE;}-*/;
@Override
public String FREESTYLE_POOMSE(){return getFREESTYLE_POOMSE();}
public final native String getKO_TOURNAMENT() /*-{	return $wnd.EditorConstants.KO_TOURNAMENT;}-*/;
@Override
public String KO_TOURNAMENT(){return getKO_TOURNAMENT();}
public final native String getLabelCompetition() /*-{	return $wnd.EditorConstants.LabelCompetition;}-*/;
@Override
public String LabelCompetition(){return getLabelCompetition();}
Map<String, String> internalTournamentModeTypeMap = null;
public void addTournamentModeTypeMapEntry(String key, String value) {	internalTournamentModeTypeMap.put(key, value);}@Override
public Map<String, String> TournamentModeTypeMap(){if (internalTournamentModeTypeMap==null){
internalTournamentModeTypeMap = new LinkedHashMap<String, String>();
String stringValues = getTournamentModeTypeMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addTournamentModeTypeMapValueForKey(valueArray[i].trim());}
}return internalTournamentModeTypeMap;}
private final native String getTournamentModeTypeMap() /*-{return $wnd.EditorConstants.TournamentModeTypeMap;}-*/;public final native void addTournamentModeTypeMapValueForKey(String key) /*-{
var value = $wnd.EditorConstants[key];
this.@com.roksoft.pocoso3.client.resources.EditorConstantsJson::addTournamentModeTypeMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
Map<String, String> internalCompetitionTypeMap = null;
public void addCompetitionTypeMapEntry(String key, String value) {	internalCompetitionTypeMap.put(key, value);}@Override
public Map<String, String> CompetitionTypeMap(){if (internalCompetitionTypeMap==null){
internalCompetitionTypeMap = new LinkedHashMap<String, String>();
String stringValues = getCompetitionTypeMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addCompetitionTypeMapValueForKey(valueArray[i].trim());}
}return internalCompetitionTypeMap;}
private final native String getCompetitionTypeMap() /*-{return $wnd.EditorConstants.CompetitionTypeMap;}-*/;public final native void addCompetitionTypeMapValueForKey(String key) /*-{
var value = $wnd.EditorConstants[key];
this.@com.roksoft.pocoso3.client.resources.EditorConstantsJson::addCompetitionTypeMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getLabelAllCompetitions() /*-{	return $wnd.EditorConstants.LabelAllCompetitions;}-*/;
@Override
public String LabelAllCompetitions(){return getLabelAllCompetitions();}

} 