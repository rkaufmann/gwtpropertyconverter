package com.roksoft.pocoso3.client.resources;
import java.util.Map;
import java.util.LinkedHashMap;
import com.google.gwt.json.client.JSONObject;
public class PosyConstantsJson extends JSONObject implements PosyConstants{ 
public final native String getEnergySliderLabel() /*-{	return $wnd.PosyConstants.EnergySliderLabel;}-*/;
@Override
public String EnergySliderLabel(){return getEnergySliderLabel();}
public final native String getLabelClubs() /*-{	return $wnd.PosyConstants.LabelClubs;}-*/;
@Override
public String LabelClubs(){return getLabelClubs();}
public final native String getLabelDownloadGooglePlay() /*-{	return $wnd.PosyConstants.LabelDownloadGooglePlay;}-*/;
@Override
public String LabelDownloadGooglePlay(){return getLabelDownloadGooglePlay();}
public final native String getStartupMenuProVersion() /*-{	return $wnd.PosyConstants.StartupMenuProVersion;}-*/;
@Override
public String StartupMenuProVersion(){return getStartupMenuProVersion();}
public final native String getDefaultLanguage() /*-{	return $wnd.PosyConstants.DefaultLanguage;}-*/;
@Override
public String DefaultLanguage(){return getDefaultLanguage();}
public final native String getLabelWaitForScore() /*-{	return $wnd.PosyConstants.LabelWaitForScore;}-*/;
@Override
public String LabelWaitForScore(){return getLabelWaitForScore();}
public final native String getMenuAdminSettingsAssignDrawing() /*-{	return $wnd.PosyConstants.MenuAdminSettingsAssignDrawing;}-*/;
@Override
public String MenuAdminSettingsAssignDrawing(){return getMenuAdminSettingsAssignDrawing();}
public final native String getButtonExportResultsTkdv() /*-{	return $wnd.PosyConstants.ButtonExportResultsTkdv;}-*/;
@Override
public String ButtonExportResultsTkdv(){return getButtonExportResultsTkdv();}
public final native String getLabelFSAccuracyOfMovements() /*-{	return $wnd.PosyConstants.LabelFSAccuracyOfMovements;}-*/;
@Override
public String LabelFSAccuracyOfMovements(){return getLabelFSAccuracyOfMovements();}
public final native String getNextButton() /*-{	return $wnd.PosyConstants.NextButton;}-*/;
@Override
public String NextButton(){return getNextButton();}
public final native String getLabelMinStartersPerRound() /*-{	return $wnd.PosyConstants.LabelMinStartersPerRound;}-*/;
@Override
public String LabelMinStartersPerRound(){return getLabelMinStartersPerRound();}
public final native String getLabelTKDVImportExport() /*-{	return $wnd.PosyConstants.LabelTKDVImportExport;}-*/;
@Override
public String LabelTKDVImportExport(){return getLabelTKDVImportExport();}
public final native String getLabelEnterUrlInBrowser() /*-{	return $wnd.PosyConstants.LabelEnterUrlInBrowser;}-*/;
@Override
public String LabelEnterUrlInBrowser(){return getLabelEnterUrlInBrowser();}
public final native String getLabelDays() /*-{	return $wnd.PosyConstants.LabelDays;}-*/;
@Override
public String LabelDays(){return getLabelDays();}
public final native String getJUDGE() /*-{	return $wnd.PosyConstants.JUDGE;}-*/;
@Override
public String JUDGE(){return getJUDGE();}
Map<String, String> internalLanguageMap = null;
public void addLanguageMapEntry(String key, String value) {	internalLanguageMap.put(key, value);}@Override
public Map<String, String> LanguageMap(){if (internalLanguageMap==null){
internalLanguageMap = new LinkedHashMap<String, String>();
String stringValues = getLanguageMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addLanguageMapValueForKey(valueArray[i].trim());}
}return internalLanguageMap;}
private final native String getLanguageMap() /*-{return $wnd.PosyConstants.LanguageMap;}-*/;public final native void addLanguageMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addLanguageMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getMenuAdminSettings() /*-{	return $wnd.PosyConstants.MenuAdminSettings;}-*/;
@Override
public String MenuAdminSettings(){return getMenuAdminSettings();}
public final native String getLabelNumberOfJudges() /*-{	return $wnd.PosyConstants.LabelNumberOfJudges;}-*/;
@Override
public String LabelNumberOfJudges(){return getLabelNumberOfJudges();}
public final native String getPrintButton() /*-{	return $wnd.PosyConstants.PrintButton;}-*/;
@Override
public String PrintButton(){return getPrintButton();}
public final native String getRESULTVIEW() /*-{	return $wnd.PosyConstants.RESULTVIEW;}-*/;
@Override
public String RESULTVIEW(){return getRESULTVIEW();}
public final native String getLabelFSHeight() /*-{	return $wnd.PosyConstants.LabelFSHeight;}-*/;
@Override
public String LabelFSHeight(){return getLabelFSHeight();}
public final native String getButtonDelete() /*-{	return $wnd.PosyConstants.ButtonDelete;}-*/;
@Override
public String ButtonDelete(){return getButtonDelete();}
public final native String getButtonAutoAssign() /*-{	return $wnd.PosyConstants.ButtonAutoAssign;}-*/;
@Override
public String ButtonAutoAssign(){return getButtonAutoAssign();}
public final native String getLabelFoundServers() /*-{	return $wnd.PosyConstants.LabelFoundServers;}-*/;
@Override
public String LabelFoundServers(){return getLabelFoundServers();}
public final native String getPassword() /*-{	return $wnd.PosyConstants.Password;}-*/;
@Override
public String Password(){return getPassword();}
public final native String getFree_Choice() /*-{	return $wnd.PosyConstants.Free_Choice;}-*/;
@Override
public String Free_Choice(){return getFree_Choice();}
public final native String getConfirmDialogTitle() /*-{	return $wnd.PosyConstants.ConfirmDialogTitle;}-*/;
@Override
public String ConfirmDialogTitle(){return getConfirmDialogTitle();}
public final native String getButtonImportFromTkdv() /*-{	return $wnd.PosyConstants.ButtonImportFromTkdv;}-*/;
@Override
public String ButtonImportFromTkdv(){return getButtonImportFromTkdv();}
public final native String getLabelConnectionInfo() /*-{	return $wnd.PosyConstants.LabelConnectionInfo;}-*/;
@Override
public String LabelConnectionInfo(){return getLabelConnectionInfo();}
public final native String getSubMenuTournamentCompetitions() /*-{	return $wnd.PosyConstants.SubMenuTournamentCompetitions;}-*/;
@Override
public String SubMenuTournamentCompetitions(){return getSubMenuTournamentCompetitions();}
public final native String getParticipantListLabel() /*-{	return $wnd.PosyConstants.ParticipantListLabel;}-*/;
@Override
public String ParticipantListLabel(){return getParticipantListLabel();}
public final native String getPoomseLabel() /*-{	return $wnd.PosyConstants.PoomseLabel;}-*/;
@Override
public String PoomseLabel(){return getPoomseLabel();}
public final native String getLabelNextStarter() /*-{	return $wnd.PosyConstants.LabelNextStarter;}-*/;
@Override
public String LabelNextStarter(){return getLabelNextStarter();}
public final native String getde() /*-{	return $wnd.PosyConstants.de;}-*/;
@Override
public String de(){return getde();}
public final native String getLabelFinal() /*-{	return $wnd.PosyConstants.LabelFinal;}-*/;
@Override
public String LabelFinal(){return getLabelFinal();}
public final native String getLabelNextForm() /*-{	return $wnd.PosyConstants.LabelNextForm;}-*/;
@Override
public String LabelNextForm(){return getLabelNextForm();}
public final native String getFreeStyle() /*-{	return $wnd.PosyConstants.FreeStyle;}-*/;
@Override
public String FreeStyle(){return getFreeStyle();}
Map<String, String> internalPoomseFormMap = null;
public void addPoomseFormMapEntry(String key, String value) {	internalPoomseFormMap.put(key, value);}@Override
public Map<String, String> PoomseFormMap(){if (internalPoomseFormMap==null){
internalPoomseFormMap = new LinkedHashMap<String, String>();
String stringValues = getPoomseFormMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addPoomseFormMapValueForKey(valueArray[i].trim());}
}return internalPoomseFormMap;}
private final native String getPoomseFormMap() /*-{return $wnd.PosyConstants.PoomseFormMap;}-*/;public final native void addPoomseFormMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addPoomseFormMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getLabelCompetitionAreas() /*-{	return $wnd.PosyConstants.LabelCompetitionAreas;}-*/;
@Override
public String LabelCompetitionAreas(){return getLabelCompetitionAreas();}
public final native String getLabelAdvanceToNextRound() /*-{	return $wnd.PosyConstants.LabelAdvanceToNextRound;}-*/;
@Override
public String LabelAdvanceToNextRound(){return getLabelAdvanceToNextRound();}
public final native String getUndoButton() /*-{	return $wnd.PosyConstants.UndoButton;}-*/;
@Override
public String UndoButton(){return getUndoButton();}
public final native String getLabelSplitIntoGroups() /*-{	return $wnd.PosyConstants.LabelSplitIntoGroups;}-*/;
@Override
public String LabelSplitIntoGroups(){return getLabelSplitIntoGroups();}
public final native String getMenuAdminSettingsCompetitionSequence() /*-{	return $wnd.PosyConstants.MenuAdminSettingsCompetitionSequence;}-*/;
@Override
public String MenuAdminSettingsCompetitionSequence(){return getMenuAdminSettingsCompetitionSequence();}
public final native String getHansu() /*-{	return $wnd.PosyConstants.Hansu;}-*/;
@Override
public String Hansu(){return getHansu();}
public final native String getPauseLabel() /*-{	return $wnd.PosyConstants.PauseLabel;}-*/;
@Override
public String PauseLabel(){return getPauseLabel();}
public final native String getLabelFSEnergy() /*-{	return $wnd.PosyConstants.LabelFSEnergy;}-*/;
@Override
public String LabelFSEnergy(){return getLabelFSEnergy();}
public final native String getButtonConnect() /*-{	return $wnd.PosyConstants.ButtonConnect;}-*/;
@Override
public String ButtonConnect(){return getButtonConnect();}
Map<String, String> internalSubMenuTournamentMap = null;
public void addSubMenuTournamentMapEntry(String key, String value) {	internalSubMenuTournamentMap.put(key, value);}@Override
public Map<String, String> SubMenuTournamentMap(){if (internalSubMenuTournamentMap==null){
internalSubMenuTournamentMap = new LinkedHashMap<String, String>();
String stringValues = getSubMenuTournamentMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addSubMenuTournamentMapValueForKey(valueArray[i].trim());}
}return internalSubMenuTournamentMap;}
private final native String getSubMenuTournamentMap() /*-{return $wnd.PosyConstants.SubMenuTournamentMap;}-*/;public final native void addSubMenuTournamentMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addSubMenuTournamentMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getSubMenuTournamentCompetitionAreas() /*-{	return $wnd.PosyConstants.SubMenuTournamentCompetitionAreas;}-*/;
@Override
public String SubMenuTournamentCompetitionAreas(){return getSubMenuTournamentCompetitionAreas();}
public final native String geten() /*-{	return $wnd.PosyConstants.en;}-*/;
@Override
public String en(){return geten();}
public final native String getPresentationlabel() /*-{	return $wnd.PosyConstants.Presentationlabel;}-*/;
@Override
public String Presentationlabel(){return getPresentationlabel();}
public final native String getLabelAssigned() /*-{	return $wnd.PosyConstants.LabelAssigned;}-*/;
@Override
public String LabelAssigned(){return getLabelAssigned();}
public final native String getLabelSemifinal() /*-{	return $wnd.PosyConstants.LabelSemifinal;}-*/;
@Override
public String LabelSemifinal(){return getLabelSemifinal();}
public final native String getLabelAttention() /*-{	return $wnd.PosyConstants.LabelAttention;}-*/;
@Override
public String LabelAttention(){return getLabelAttention();}
public final native String getLabelDownloadLocal() /*-{	return $wnd.PosyConstants.LabelDownloadLocal;}-*/;
@Override
public String LabelDownloadLocal(){return getLabelDownloadLocal();}
public final native String getStartupMenuWebSite() /*-{	return $wnd.PosyConstants.StartupMenuWebSite;}-*/;
@Override
public String StartupMenuWebSite(){return getStartupMenuWebSite();}
public final native String getRankingListLabel() /*-{	return $wnd.PosyConstants.RankingListLabel;}-*/;
@Override
public String RankingListLabel(){return getRankingListLabel();}
public final native String getBigak() /*-{	return $wnd.PosyConstants.Bigak;}-*/;
@Override
public String Bigak(){return getBigak();}
public final native String getLabelTimePenalty() /*-{	return $wnd.PosyConstants.LabelTimePenalty;}-*/;
@Override
public String LabelTimePenalty(){return getLabelTimePenalty();}
public final native String getAREAADMIN() /*-{	return $wnd.PosyConstants.AREAADMIN;}-*/;
@Override
public String AREAADMIN(){return getAREAADMIN();}
public final native String getLabelPreliminary() /*-{	return $wnd.PosyConstants.LabelPreliminary;}-*/;
@Override
public String LabelPreliminary(){return getLabelPreliminary();}
public final native String getNextCompetition() /*-{	return $wnd.PosyConstants.NextCompetition;}-*/;
@Override
public String NextCompetition(){return getNextCompetition();}
public final native String getErrorNoCompetitionStarted() /*-{	return $wnd.PosyConstants.ErrorNoCompetitionStarted;}-*/;
@Override
public String ErrorNoCompetitionStarted(){return getErrorNoCompetitionStarted();}
public final native String getLabelAndroidAppConnectionsIssues() /*-{	return $wnd.PosyConstants.LabelAndroidAppConnectionsIssues;}-*/;
@Override
public String LabelAndroidAppConnectionsIssues(){return getLabelAndroidAppConnectionsIssues();}
public final native String getLabelIgnoreLowest() /*-{	return $wnd.PosyConstants.LabelIgnoreLowest;}-*/;
@Override
public String LabelIgnoreLowest(){return getLabelIgnoreLowest();}
public final native String getLabelRoundSettings() /*-{	return $wnd.PosyConstants.LabelRoundSettings;}-*/;
@Override
public String LabelRoundSettings(){return getLabelRoundSettings();}
public final native String getSubMenuCompetitionsMembers() /*-{	return $wnd.PosyConstants.SubMenuCompetitionsMembers;}-*/;
@Override
public String SubMenuCompetitionsMembers(){return getSubMenuCompetitionsMembers();}
public final native String getRankingDonePerAllRoundsLabel() /*-{	return $wnd.PosyConstants.RankingDonePerAllRoundsLabel;}-*/;
@Override
public String RankingDonePerAllRoundsLabel(){return getRankingDonePerAllRoundsLabel();}
public final native String getLabelTournamentMode() /*-{	return $wnd.PosyConstants.LabelTournamentMode;}-*/;
@Override
public String LabelTournamentMode(){return getLabelTournamentMode();}
public final native String getPyongwon() /*-{	return $wnd.PosyConstants.Pyongwon;}-*/;
@Override
public String Pyongwon(){return getPyongwon();}
public final native String getLabelFSPracticability() /*-{	return $wnd.PosyConstants.LabelFSPracticability;}-*/;
@Override
public String LabelFSPracticability(){return getLabelFSPracticability();}
public final native String getLabelNothingFound() /*-{	return $wnd.PosyConstants.LabelNothingFound;}-*/;
@Override
public String LabelNothingFound(){return getLabelNothingFound();}
public final native String getLabelFSCreativeness() /*-{	return $wnd.PosyConstants.LabelFSCreativeness;}-*/;
@Override
public String LabelFSCreativeness(){return getLabelFSCreativeness();}
public final native String getLabelTournamentDay() /*-{	return $wnd.PosyConstants.LabelTournamentDay;}-*/;
@Override
public String LabelTournamentDay(){return getLabelTournamentDay();}
public final native String getTaebaek() /*-{	return $wnd.PosyConstants.Taebaek;}-*/;
@Override
public String Taebaek(){return getTaebaek();}
public final native String getLabelCompetitionType() /*-{	return $wnd.PosyConstants.LabelCompetitionType;}-*/;
@Override
public String LabelCompetitionType(){return getLabelCompetitionType();}
public final native String getButtonSkipStarter() /*-{	return $wnd.PosyConstants.ButtonSkipStarter;}-*/;
@Override
public String ButtonSkipStarter(){return getButtonSkipStarter();}
public final native String getStartupMenuAbout() /*-{	return $wnd.PosyConstants.StartupMenuAbout;}-*/;
@Override
public String StartupMenuAbout(){return getStartupMenuAbout();}
public final native String getSaveButton() /*-{	return $wnd.PosyConstants.SaveButton;}-*/;
@Override
public String SaveButton(){return getSaveButton();}
public final native String getLabelSex() /*-{	return $wnd.PosyConstants.LabelSex;}-*/;
@Override
public String LabelSex(){return getLabelSex();}
public final native String getAppTitle() /*-{	return $wnd.PosyConstants.AppTitle;}-*/;
@Override
public String AppTitle(){return getAppTitle();}
public final native String getcs() /*-{	return $wnd.PosyConstants.cs;}-*/;
@Override
public String cs(){return getcs();}
public final native String getMessageAutoAssignSuccessful() /*-{	return $wnd.PosyConstants.MessageAutoAssignSuccessful;}-*/;
@Override
public String MessageAutoAssignSuccessful(){return getMessageAutoAssignSuccessful();}
public final native String getLabelFormsPerParticipant() /*-{	return $wnd.PosyConstants.LabelFormsPerParticipant;}-*/;
@Override
public String LabelFormsPerParticipant(){return getLabelFormsPerParticipant();}
public final native String getLabelNumberOfStarters() /*-{	return $wnd.PosyConstants.LabelNumberOfStarters;}-*/;
@Override
public String LabelNumberOfStarters(){return getLabelNumberOfStarters();}
public final native String getMenuAdminSettingsArea() /*-{	return $wnd.PosyConstants.MenuAdminSettingsArea;}-*/;
@Override
public String MenuAdminSettingsArea(){return getMenuAdminSettingsArea();}
public final native String getLabelPauseTime() /*-{	return $wnd.PosyConstants.LabelPauseTime;}-*/;
@Override
public String LabelPauseTime(){return getLabelPauseTime();}
public final native String getLabelInfo() /*-{	return $wnd.PosyConstants.LabelInfo;}-*/;
@Override
public String LabelInfo(){return getLabelInfo();}
public final native String getMenuAdminSettingsDrawing() /*-{	return $wnd.PosyConstants.MenuAdminSettingsDrawing;}-*/;
@Override
public String MenuAdminSettingsDrawing(){return getMenuAdminSettingsDrawing();}
public final native String getLanguageTitle() /*-{	return $wnd.PosyConstants.LanguageTitle;}-*/;
@Override
public String LanguageTitle(){return getLanguageTitle();}
public final native String getLastUsed() /*-{	return $wnd.PosyConstants.LastUsed;}-*/;
@Override
public String LastUsed(){return getLastUsed();}
public final native String getWelcome() /*-{	return $wnd.PosyConstants.Welcome;}-*/;
@Override
public String Welcome(){return getWelcome();}
public final native String getMenuAdminSettingsRound() /*-{	return $wnd.PosyConstants.MenuAdminSettingsRound;}-*/;
@Override
public String MenuAdminSettingsRound(){return getMenuAdminSettingsRound();}
public final native String getMessageSaveOk() /*-{	return $wnd.PosyConstants.MessageSaveOk;}-*/;
@Override
public String MessageSaveOk(){return getMessageSaveOk();}
public final native String getLabelCurrentRound() /*-{	return $wnd.PosyConstants.LabelCurrentRound;}-*/;
@Override
public String LabelCurrentRound(){return getLabelCurrentRound();}
public final native String getSPECTATOR() /*-{	return $wnd.PosyConstants.SPECTATOR;}-*/;
@Override
public String SPECTATOR(){return getSPECTATOR();}
public final native String getLabelAboutPocoso() /*-{	return $wnd.PosyConstants.LabelAboutPocoso;}-*/;
@Override
public String LabelAboutPocoso(){return getLabelAboutPocoso();}
public final native String getSubMenuTournamentGeneral() /*-{	return $wnd.PosyConstants.SubMenuTournamentGeneral;}-*/;
@Override
public String SubMenuTournamentGeneral(){return getSubMenuTournamentGeneral();}
public final native String getRythmSliderLabel() /*-{	return $wnd.PosyConstants.RythmSliderLabel;}-*/;
@Override
public String RythmSliderLabel(){return getRythmSliderLabel();}
public final native String getLabelNumberOfRoundsFixed() /*-{	return $wnd.PosyConstants.LabelNumberOfRoundsFixed;}-*/;
@Override
public String LabelNumberOfRoundsFixed(){return getLabelNumberOfRoundsFixed();}
Map<String, String> internalUserRoleMap = null;
public void addUserRoleMapEntry(String key, String value) {	internalUserRoleMap.put(key, value);}@Override
public Map<String, String> UserRoleMap(){if (internalUserRoleMap==null){
internalUserRoleMap = new LinkedHashMap<String, String>();
String stringValues = getUserRoleMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addUserRoleMapValueForKey(valueArray[i].trim());}
}return internalUserRoleMap;}
private final native String getUserRoleMap() /*-{return $wnd.PosyConstants.UserRoleMap;}-*/;public final native void addUserRoleMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addUserRoleMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getHanryu() /*-{	return $wnd.PosyConstants.Hanryu;}-*/;
@Override
public String Hanryu(){return getHanryu();}
public final native String getLabelBorderPenalty() /*-{	return $wnd.PosyConstants.LabelBorderPenalty;}-*/;
@Override
public String LabelBorderPenalty(){return getLabelBorderPenalty();}
public final native String getLabelFormsPerRound() /*-{	return $wnd.PosyConstants.LabelFormsPerRound;}-*/;
@Override
public String LabelFormsPerRound(){return getLabelFormsPerRound();}
public final native String getContinueButton() /*-{	return $wnd.PosyConstants.ContinueButton;}-*/;
@Override
public String ContinueButton(){return getContinueButton();}
public final native String getLabelLocation() /*-{	return $wnd.PosyConstants.LabelLocation;}-*/;
@Override
public String LabelLocation(){return getLabelLocation();}
public final native String getQuestionDelete() /*-{	return $wnd.PosyConstants.QuestionDelete;}-*/;
@Override
public String QuestionDelete(){return getQuestionDelete();}
public final native String getLabelNumberOfRounds() /*-{	return $wnd.PosyConstants.LabelNumberOfRounds;}-*/;
@Override
public String LabelNumberOfRounds(){return getLabelNumberOfRounds();}
public final native String getLoginTitle() /*-{	return $wnd.PosyConstants.LoginTitle;}-*/;
@Override
public String LoginTitle(){return getLoginTitle();}
public final native String getLabelCompletion() /*-{	return $wnd.PosyConstants.LabelCompletion;}-*/;
@Override
public String LabelCompletion(){return getLabelCompletion();}
public final native String getLabelCompetitionSearchFilter() /*-{	return $wnd.PosyConstants.LabelCompetitionSearchFilter;}-*/;
@Override
public String LabelCompetitionSearchFilter(){return getLabelCompetitionSearchFilter();}
public final native String getStartupMenuDownloadApp() /*-{	return $wnd.PosyConstants.StartupMenuDownloadApp;}-*/;
@Override
public String StartupMenuDownloadApp(){return getStartupMenuDownloadApp();}
public final native String getpt() /*-{	return $wnd.PosyConstants.pt;}-*/;
@Override
public String pt(){return getpt();}
public final native String getLabelFSGradientTurns() /*-{	return $wnd.PosyConstants.LabelFSGradientTurns;}-*/;
@Override
public String LabelFSGradientTurns(){return getLabelFSGradientTurns();}
public final native String getLabelImportFromExcel() /*-{	return $wnd.PosyConstants.LabelImportFromExcel;}-*/;
@Override
public String LabelImportFromExcel(){return getLabelImportFromExcel();}
public final native String getControlSliderLabel() /*-{	return $wnd.PosyConstants.ControlSliderLabel;}-*/;
@Override
public String ControlSliderLabel(){return getControlSliderLabel();}
Map<String, String> internalSubMenuClubsMap = null;
public void addSubMenuClubsMapEntry(String key, String value) {	internalSubMenuClubsMap.put(key, value);}@Override
public Map<String, String> SubMenuClubsMap(){if (internalSubMenuClubsMap==null){
internalSubMenuClubsMap = new LinkedHashMap<String, String>();
String stringValues = getSubMenuClubsMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addSubMenuClubsMapValueForKey(valueArray[i].trim());}
}return internalSubMenuClubsMap;}
private final native String getSubMenuClubsMap() /*-{return $wnd.PosyConstants.SubMenuClubsMap;}-*/;public final native void addSubMenuClubsMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addSubMenuClubsMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getButtonTimerStart() /*-{	return $wnd.PosyConstants.ButtonTimerStart;}-*/;
@Override
public String ButtonTimerStart(){return getButtonTimerStart();}
public final native String getLabelConnectionCode() /*-{	return $wnd.PosyConstants.LabelConnectionCode;}-*/;
@Override
public String LabelConnectionCode(){return getLabelConnectionCode();}
public final native String getLabelNationality() /*-{	return $wnd.PosyConstants.LabelNationality;}-*/;
@Override
public String LabelNationality(){return getLabelNationality();}
public final native String getLabelPort() /*-{	return $wnd.PosyConstants.LabelPort;}-*/;
@Override
public String LabelPort(){return getLabelPort();}
public final native String getLabelFSNumberConsecutiveKicks() /*-{	return $wnd.PosyConstants.LabelFSNumberConsecutiveKicks;}-*/;
@Override
public String LabelFSNumberConsecutiveKicks(){return getLabelFSNumberConsecutiveKicks();}
public final native String getErrorNoDaySelected() /*-{	return $wnd.PosyConstants.ErrorNoDaySelected;}-*/;
@Override
public String ErrorNoDaySelected(){return getErrorNoDaySelected();}
public final native String getIlyo() /*-{	return $wnd.PosyConstants.Ilyo;}-*/;
@Override
public String Ilyo(){return getIlyo();}
public final native String getSubMenuClubsGeneral() /*-{	return $wnd.PosyConstants.SubMenuClubsGeneral;}-*/;
@Override
public String SubMenuClubsGeneral(){return getSubMenuClubsGeneral();}
public final native String getQuestionExistingAssignmentsOverwritten() /*-{	return $wnd.PosyConstants.QuestionExistingAssignmentsOverwritten;}-*/;
@Override
public String QuestionExistingAssignmentsOverwritten(){return getQuestionExistingAssignmentsOverwritten();}
public final native String getLoginButton() /*-{	return $wnd.PosyConstants.LoginButton;}-*/;
@Override
public String LoginButton(){return getLoginButton();}
public final native String getLabelNotAssigned() /*-{	return $wnd.PosyConstants.LabelNotAssigned;}-*/;
@Override
public String LabelNotAssigned(){return getLabelNotAssigned();}
public final native String getLabelQuestionResetOrContinueDemoCompetition() /*-{	return $wnd.PosyConstants.LabelQuestionResetOrContinueDemoCompetition;}-*/;
@Override
public String LabelQuestionResetOrContinueDemoCompetition(){return getLabelQuestionResetOrContinueDemoCompetition();}
Map<String, String> internalMenuDataMap = null;
public void addMenuDataMapEntry(String key, String value) {	internalMenuDataMap.put(key, value);}@Override
public Map<String, String> MenuDataMap(){if (internalMenuDataMap==null){
internalMenuDataMap = new LinkedHashMap<String, String>();
String stringValues = getMenuDataMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addMenuDataMapValueForKey(valueArray[i].trim());}
}return internalMenuDataMap;}
private final native String getMenuDataMap() /*-{return $wnd.PosyConstants.MenuDataMap;}-*/;public final native void addMenuDataMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addMenuDataMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getButtonNone() /*-{	return $wnd.PosyConstants.ButtonNone;}-*/;
@Override
public String ButtonNone(){return getButtonNone();}
public final native String getLabelTournaments() /*-{	return $wnd.PosyConstants.LabelTournaments;}-*/;
@Override
public String LabelTournaments(){return getLabelTournaments();}
public final native String getLabelExportToExcel() /*-{	return $wnd.PosyConstants.LabelExportToExcel;}-*/;
@Override
public String LabelExportToExcel(){return getLabelExportToExcel();}
public final native String getAverageLabel() /*-{	return $wnd.PosyConstants.AverageLabel;}-*/;
@Override
public String AverageLabel(){return getAverageLabel();}
public final native String getLabelFSChoreography() /*-{	return $wnd.PosyConstants.LabelFSChoreography;}-*/;
@Override
public String LabelFSChoreography(){return getLabelFSChoreography();}
public final native String getLabelNavigation() /*-{	return $wnd.PosyConstants.LabelNavigation;}-*/;
@Override
public String LabelNavigation(){return getLabelNavigation();}
public final native String getCorrectButton() /*-{	return $wnd.PosyConstants.CorrectButton;}-*/;
@Override
public String CorrectButton(){return getCorrectButton();}
public final native String getSipjin() /*-{	return $wnd.PosyConstants.Sipjin;}-*/;
@Override
public String Sipjin(){return getSipjin();}
public final native String getADMIN() /*-{	return $wnd.PosyConstants.ADMIN;}-*/;
@Override
public String ADMIN(){return getADMIN();}
public final native String getLabelCurrentStarter() /*-{	return $wnd.PosyConstants.LabelCurrentStarter;}-*/;
@Override
public String LabelCurrentStarter(){return getLabelCurrentStarter();}
public final native String getLabelCompetitions() /*-{	return $wnd.PosyConstants.LabelCompetitions;}-*/;
@Override
public String LabelCompetitions(){return getLabelCompetitions();}
public final native String getLabelFoundRoundSettings() /*-{	return $wnd.PosyConstants.LabelFoundRoundSettings;}-*/;
@Override
public String LabelFoundRoundSettings(){return getLabelFoundRoundSettings();}
public final native String getTotalLabel() /*-{	return $wnd.PosyConstants.TotalLabel;}-*/;
@Override
public String TotalLabel(){return getTotalLabel();}
public final native String getLabelError() /*-{	return $wnd.PosyConstants.LabelError;}-*/;
@Override
public String LabelError(){return getLabelError();}
public final native String getBackButton() /*-{	return $wnd.PosyConstants.BackButton;}-*/;
@Override
public String BackButton(){return getBackButton();}
public final native String getMenuDataTournament() /*-{	return $wnd.PosyConstants.MenuDataTournament;}-*/;
@Override
public String MenuDataTournament(){return getMenuDataTournament();}
public final native String getPleaseWaitLabel() /*-{	return $wnd.PosyConstants.PleaseWaitLabel;}-*/;
@Override
public String PleaseWaitLabel(){return getPleaseWaitLabel();}
public final native String getLabelTime() /*-{	return $wnd.PosyConstants.LabelTime;}-*/;
@Override
public String LabelTime(){return getLabelTime();}
public final native String getLabelIgnoreHighest() /*-{	return $wnd.PosyConstants.LabelIgnoreHighest;}-*/;
@Override
public String LabelIgnoreHighest(){return getLabelIgnoreHighest();}
public final native String getLabelClubMembers() /*-{	return $wnd.PosyConstants.LabelClubMembers;}-*/;
@Override
public String LabelClubMembers(){return getLabelClubMembers();}
public final native String getMenuAdminSettingsAreaCompetition() /*-{	return $wnd.PosyConstants.MenuAdminSettingsAreaCompetition;}-*/;
@Override
public String MenuAdminSettingsAreaCompetition(){return getMenuAdminSettingsAreaCompetition();}
public final native String getLabelFSCompletionFootTechniques() /*-{	return $wnd.PosyConstants.LabelFSCompletionFootTechniques;}-*/;
@Override
public String LabelFSCompletionFootTechniques(){return getLabelFSCompletionFootTechniques();}
public final native String getAccuracyLabel() /*-{	return $wnd.PosyConstants.AccuracyLabel;}-*/;
@Override
public String AccuracyLabel(){return getAccuracyLabel();}
public final native String getMenuDataParticipants() /*-{	return $wnd.PosyConstants.MenuDataParticipants;}-*/;
@Override
public String MenuDataParticipants(){return getMenuDataParticipants();}
public final native String getLabelFSPoomsaeCompletion() /*-{	return $wnd.PosyConstants.LabelFSPoomsaeCompletion;}-*/;
@Override
public String LabelFSPoomsaeCompletion(){return getLabelFSPoomsaeCompletion();}
public final native String getLabelMoveToNextRound() /*-{	return $wnd.PosyConstants.LabelMoveToNextRound;}-*/;
@Override
public String LabelMoveToNextRound(){return getLabelMoveToNextRound();}
public final native String getLabelClub() /*-{	return $wnd.PosyConstants.LabelClub;}-*/;
@Override
public String LabelClub(){return getLabelClub();}
public final native String getButtonExportResultsExcel() /*-{	return $wnd.PosyConstants.ButtonExportResultsExcel;}-*/;
@Override
public String ButtonExportResultsExcel(){return getButtonExportResultsExcel();}
public final native String getRankLabel() /*-{	return $wnd.PosyConstants.RankLabel;}-*/;
@Override
public String RankLabel(){return getRankLabel();}
public final native String getLabelAccuracy() /*-{	return $wnd.PosyConstants.LabelAccuracy;}-*/;
@Override
public String LabelAccuracy(){return getLabelAccuracy();}
public final native String getLabelTop() /*-{	return $wnd.PosyConstants.LabelTop;}-*/;
@Override
public String LabelTop(){return getLabelTop();}
public final native String getLabelGeneralSettingsDemo() /*-{	return $wnd.PosyConstants.LabelGeneralSettingsDemo;}-*/;
@Override
public String LabelGeneralSettingsDemo(){return getLabelGeneralSettingsDemo();}
public final native String getStartButton() /*-{	return $wnd.PosyConstants.StartButton;}-*/;
@Override
public String StartButton(){return getStartButton();}
public final native String getLabelScores() /*-{	return $wnd.PosyConstants.LabelScores;}-*/;
@Override
public String LabelScores(){return getLabelScores();}
public final native String getLabelFSAcrobaticActions() /*-{	return $wnd.PosyConstants.LabelFSAcrobaticActions;}-*/;
@Override
public String LabelFSAcrobaticActions(){return getLabelFSAcrobaticActions();}
public final native String getCorrectQuestion() /*-{	return $wnd.PosyConstants.CorrectQuestion;}-*/;
@Override
public String CorrectQuestion(){return getCorrectQuestion();}
public final native String getLabelRanks() /*-{	return $wnd.PosyConstants.LabelRanks;}-*/;
@Override
public String LabelRanks(){return getLabelRanks();}
public final native String getTaeguk8() /*-{	return $wnd.PosyConstants.Taeguk8;}-*/;
@Override
public String Taeguk8(){return getTaeguk8();}
public final native String getTaeguk7() /*-{	return $wnd.PosyConstants.Taeguk7;}-*/;
@Override
public String Taeguk7(){return getTaeguk7();}
public final native String getTaeguk6() /*-{	return $wnd.PosyConstants.Taeguk6;}-*/;
@Override
public String Taeguk6(){return getTaeguk6();}
public final native String getStartupMenuFreeVersion() /*-{	return $wnd.PosyConstants.StartupMenuFreeVersion;}-*/;
@Override
public String StartupMenuFreeVersion(){return getStartupMenuFreeVersion();}
public final native String getSubMenuCompetitionsGeneral() /*-{	return $wnd.PosyConstants.SubMenuCompetitionsGeneral;}-*/;
@Override
public String SubMenuCompetitionsGeneral(){return getSubMenuCompetitionsGeneral();}
public final native String getTaeguk5() /*-{	return $wnd.PosyConstants.Taeguk5;}-*/;
@Override
public String Taeguk5(){return getTaeguk5();}
public final native String getButtonTimerReset() /*-{	return $wnd.PosyConstants.ButtonTimerReset;}-*/;
@Override
public String ButtonTimerReset(){return getButtonTimerReset();}
public final native String getLabelCurrentForm() /*-{	return $wnd.PosyConstants.LabelCurrentForm;}-*/;
@Override
public String LabelCurrentForm(){return getLabelCurrentForm();}
public final native String getKumgang() /*-{	return $wnd.PosyConstants.Kumgang;}-*/;
@Override
public String Kumgang(){return getKumgang();}
public final native String getLabelCompetitionArea() /*-{	return $wnd.PosyConstants.LabelCompetitionArea;}-*/;
@Override
public String LabelCompetitionArea(){return getLabelCompetitionArea();}
public final native String getLabelTechnicalSkills() /*-{	return $wnd.PosyConstants.LabelTechnicalSkills;}-*/;
@Override
public String LabelTechnicalSkills(){return getLabelTechnicalSkills();}
public final native String getWaitForStarterMessage() /*-{	return $wnd.PosyConstants.WaitForStarterMessage;}-*/;
@Override
public String WaitForStarterMessage(){return getWaitForStarterMessage();}
Map<String, String> internalSubMenuCompetitionsMap = null;
public void addSubMenuCompetitionsMapEntry(String key, String value) {	internalSubMenuCompetitionsMap.put(key, value);}@Override
public Map<String, String> SubMenuCompetitionsMap(){if (internalSubMenuCompetitionsMap==null){
internalSubMenuCompetitionsMap = new LinkedHashMap<String, String>();
String stringValues = getSubMenuCompetitionsMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addSubMenuCompetitionsMapValueForKey(valueArray[i].trim());}
}return internalSubMenuCompetitionsMap;}
private final native String getSubMenuCompetitionsMap() /*-{return $wnd.PosyConstants.SubMenuCompetitionsMap;}-*/;public final native void addSubMenuCompetitionsMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addSubMenuCompetitionsMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
Map<String, String> internalStartupMenuMap = null;
public void addStartupMenuMapEntry(String key, String value) {	internalStartupMenuMap.put(key, value);}@Override
public Map<String, String> StartupMenuMap(){if (internalStartupMenuMap==null){
internalStartupMenuMap = new LinkedHashMap<String, String>();
String stringValues = getStartupMenuMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addStartupMenuMapValueForKey(valueArray[i].trim());}
}return internalStartupMenuMap;}
private final native String getStartupMenuMap() /*-{return $wnd.PosyConstants.StartupMenuMap;}-*/;public final native void addStartupMenuMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addStartupMenuMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getResultViewTitle() /*-{	return $wnd.PosyConstants.ResultViewTitle;}-*/;
@Override
public String ResultViewTitle(){return getResultViewTitle();}
public final native String getTaeguk3() /*-{	return $wnd.PosyConstants.Taeguk3;}-*/;
@Override
public String Taeguk3(){return getTaeguk3();}
public final native String getTaeguk4() /*-{	return $wnd.PosyConstants.Taeguk4;}-*/;
@Override
public String Taeguk4(){return getTaeguk4();}
public final native String getLabelRoundTime() /*-{	return $wnd.PosyConstants.LabelRoundTime;}-*/;
@Override
public String LabelRoundTime(){return getLabelRoundTime();}
public final native String getLabelConsiderOnlyLastRound() /*-{	return $wnd.PosyConstants.LabelConsiderOnlyLastRound;}-*/;
@Override
public String LabelConsiderOnlyLastRound(){return getLabelConsiderOnlyLastRound();}
public final native String getTaeguk1() /*-{	return $wnd.PosyConstants.Taeguk1;}-*/;
@Override
public String Taeguk1(){return getTaeguk1();}
public final native String getTaeguk2() /*-{	return $wnd.PosyConstants.Taeguk2;}-*/;
@Override
public String Taeguk2(){return getTaeguk2();}
public final native String getLabelHost() /*-{	return $wnd.PosyConstants.LabelHost;}-*/;
@Override
public String LabelHost(){return getLabelHost();}
public final native String getMenuDataClubs() /*-{	return $wnd.PosyConstants.MenuDataClubs;}-*/;
@Override
public String MenuDataClubs(){return getMenuDataClubs();}
public final native String getStartupMenuLiveTicker() /*-{	return $wnd.PosyConstants.StartupMenuLiveTicker;}-*/;
@Override
public String StartupMenuLiveTicker(){return getStartupMenuLiveTicker();}
public final native String getButtonExportStartlistExcel() /*-{	return $wnd.PosyConstants.ButtonExportStartlistExcel;}-*/;
@Override
public String ButtonExportStartlistExcel(){return getButtonExportStartlistExcel();}
public final native String getRoundLabel() /*-{	return $wnd.PosyConstants.RoundLabel;}-*/;
@Override
public String RoundLabel(){return getRoundLabel();}
public final native String getMessageServerNotReachable() /*-{	return $wnd.PosyConstants.MessageServerNotReachable;}-*/;
@Override
public String MessageServerNotReachable(){return getMessageServerNotReachable();}
public final native String getCancelButton() /*-{	return $wnd.PosyConstants.CancelButton;}-*/;
@Override
public String CancelButton(){return getCancelButton();}
public final native String getLabelRoundsPerCompetition() /*-{	return $wnd.PosyConstants.LabelRoundsPerCompetition;}-*/;
@Override
public String LabelRoundsPerCompetition(){return getLabelRoundsPerCompetition();}
public final native String getSubMenuClubsMembers() /*-{	return $wnd.PosyConstants.SubMenuClubsMembers;}-*/;
@Override
public String SubMenuClubsMembers(){return getSubMenuClubsMembers();}
public final native String getMenuAdminSettingsImport() /*-{	return $wnd.PosyConstants.MenuAdminSettingsImport;}-*/;
@Override
public String MenuAdminSettingsImport(){return getMenuAdminSettingsImport();}
public final native String getGenericStopQuestion() /*-{	return $wnd.PosyConstants.GenericStopQuestion;}-*/;
@Override
public String GenericStopQuestion(){return getGenericStopQuestion();}
public final native String getJitae() /*-{	return $wnd.PosyConstants.Jitae;}-*/;
@Override
public String Jitae(){return getJitae();}
public final native String getLabelFootTechnique() /*-{	return $wnd.PosyConstants.LabelFootTechnique;}-*/;
@Override
public String LabelFootTechnique(){return getLabelFootTechnique();}
public final native String getMenuAdminSettingsAssignment() /*-{	return $wnd.PosyConstants.MenuAdminSettingsAssignment;}-*/;
@Override
public String MenuAdminSettingsAssignment(){return getMenuAdminSettingsAssignment();}
public final native String getLabelDescription() /*-{	return $wnd.PosyConstants.LabelDescription;}-*/;
@Override
public String LabelDescription(){return getLabelDescription();}
public final native String getNameLabel() /*-{	return $wnd.PosyConstants.NameLabel;}-*/;
@Override
public String NameLabel(){return getNameLabel();}
public final native String getMenuDataUsers() /*-{	return $wnd.PosyConstants.MenuDataUsers;}-*/;
@Override
public String MenuDataUsers(){return getMenuDataUsers();}
public final native String getQuestionSkipStarter() /*-{	return $wnd.PosyConstants.QuestionSkipStarter;}-*/;
@Override
public String QuestionSkipStarter(){return getQuestionSkipStarter();}
public final native String getLabelPocosoServer() /*-{	return $wnd.PosyConstants.LabelPocosoServer;}-*/;
@Override
public String LabelPocosoServer(){return getLabelPocosoServer();}
public final native String getCompulsory_Poomsae() /*-{	return $wnd.PosyConstants.Compulsory_Poomsae;}-*/;
@Override
public String Compulsory_Poomsae(){return getCompulsory_Poomsae();}
public final native String getLabelSequence() /*-{	return $wnd.PosyConstants.LabelSequence;}-*/;
@Override
public String LabelSequence(){return getLabelSequence();}
public final native String getReloadButton() /*-{	return $wnd.PosyConstants.ReloadButton;}-*/;
@Override
public String ReloadButton(){return getReloadButton();}
Map<String, String> internalMenuAdminMainMap = null;
public void addMenuAdminMainMapEntry(String key, String value) {	internalMenuAdminMainMap.put(key, value);}@Override
public Map<String, String> MenuAdminMainMap(){if (internalMenuAdminMainMap==null){
internalMenuAdminMainMap = new LinkedHashMap<String, String>();
String stringValues = getMenuAdminMainMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addMenuAdminMainMapValueForKey(valueArray[i].trim());}
}return internalMenuAdminMainMap;}
private final native String getMenuAdminMainMap() /*-{return $wnd.PosyConstants.MenuAdminMainMap;}-*/;public final native void addMenuAdminMainMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addMenuAdminMainMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getMenuAdminSettingsParticipantSequence() /*-{	return $wnd.PosyConstants.MenuAdminSettingsParticipantSequence;}-*/;
@Override
public String MenuAdminSettingsParticipantSequence(){return getMenuAdminSettingsParticipantSequence();}
public final native String getQuestionNextForm() /*-{	return $wnd.PosyConstants.QuestionNextForm;}-*/;
@Override
public String QuestionNextForm(){return getQuestionNextForm();}
public final native String getRankingDonePerLastRoundLabel() /*-{	return $wnd.PosyConstants.RankingDonePerLastRoundLabel;}-*/;
@Override
public String RankingDonePerLastRoundLabel(){return getRankingDonePerLastRoundLabel();}
public final native String getLabelFSNumberJumpKicks() /*-{	return $wnd.PosyConstants.LabelFSNumberJumpKicks;}-*/;
@Override
public String LabelFSNumberJumpKicks(){return getLabelFSNumberJumpKicks();}
public final native String getKoryo() /*-{	return $wnd.PosyConstants.Koryo;}-*/;
@Override
public String Koryo(){return getKoryo();}
Map<String, String> internalMenuAdminSettingsMap = null;
public void addMenuAdminSettingsMapEntry(String key, String value) {	internalMenuAdminSettingsMap.put(key, value);}@Override
public Map<String, String> MenuAdminSettingsMap(){if (internalMenuAdminSettingsMap==null){
internalMenuAdminSettingsMap = new LinkedHashMap<String, String>();
String stringValues = getMenuAdminSettingsMap();
String[] valueArray = stringValues.trim().split(",");
for (int i = 0; i < valueArray.length; i++)
{addMenuAdminSettingsMapValueForKey(valueArray[i].trim());}
}return internalMenuAdminSettingsMap;}
private final native String getMenuAdminSettingsMap() /*-{return $wnd.PosyConstants.MenuAdminSettingsMap;}-*/;public final native void addMenuAdminSettingsMapValueForKey(String key) /*-{
var value = $wnd.PosyConstants[key];
this.@com.roksoft.pocoso3.client.resources.PosyConstantsJson::addMenuAdminSettingsMapEntry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;
public final native String getLabelBirthday() /*-{	return $wnd.PosyConstants.LabelBirthday;}-*/;
@Override
public String LabelBirthday(){return getLabelBirthday();}
public final native String getButtonImport() /*-{	return $wnd.PosyConstants.ButtonImport;}-*/;
@Override
public String ButtonImport(){return getButtonImport();}
public final native String getWaitLabel() /*-{	return $wnd.PosyConstants.WaitLabel;}-*/;
@Override
public String WaitLabel(){return getWaitLabel();}
public final native String getChonkwon() /*-{	return $wnd.PosyConstants.Chonkwon;}-*/;
@Override
public String Chonkwon(){return getChonkwon();}
public final native String getLabelScore() /*-{	return $wnd.PosyConstants.LabelScore;}-*/;
@Override
public String LabelScore(){return getLabelScore();}
public final native String getLabelFSChoreographyExpression() /*-{	return $wnd.PosyConstants.LabelFSChoreographyExpression;}-*/;
@Override
public String LabelFSChoreographyExpression(){return getLabelFSChoreographyExpression();}
public final native String getLabelScanQRCode() /*-{	return $wnd.PosyConstants.LabelScanQRCode;}-*/;
@Override
public String LabelScanQRCode(){return getLabelScanQRCode();}
public final native String getResetButton() /*-{	return $wnd.PosyConstants.ResetButton;}-*/;
@Override
public String ResetButton(){return getResetButton();}
public final native String getMenuAdminData() /*-{	return $wnd.PosyConstants.MenuAdminData;}-*/;
@Override
public String MenuAdminData(){return getMenuAdminData();}
public final native String getLabelAndroidAppInfo() /*-{	return $wnd.PosyConstants.LabelAndroidAppInfo;}-*/;
@Override
public String LabelAndroidAppInfo(){return getLabelAndroidAppInfo();}
public final native String getLogoutButton() /*-{	return $wnd.PosyConstants.LogoutButton;}-*/;
@Override
public String LogoutButton(){return getLogoutButton();}
public final native String getLabelGeneralSettings() /*-{	return $wnd.PosyConstants.LabelGeneralSettings;}-*/;
@Override
public String LabelGeneralSettings(){return getLabelGeneralSettings();}
public final native String getButtonAll() /*-{	return $wnd.PosyConstants.ButtonAll;}-*/;
@Override
public String ButtonAll(){return getButtonAll();}

} 