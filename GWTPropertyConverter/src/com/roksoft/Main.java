/**
 * 
 */
package com.roksoft;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.json.JSONObject;
import org.json.Property;

/**
 * @author Protoss78
 */
public class Main
{
	private static final String PACKAGE_NAME = "package com.roksoft.pocoso3.client.resources;\n";
	public static final String properties = ".properties";
	public static final String json = ".js";
	public static final String[] fileNames = { "CountryConstants", "EditorConstants", "PosyConstants" };
	public static final String[] languages = { "en", "de", "cs", "pt" };
	public static final String propertyPath = "./input";
	public static final String jsonPath = "./output";

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		new Main().convertAllPropertyFiles();
	}

	/**
	 * 
	 */
	public void convertAllPropertyFiles()
	{
		for (int i = 0; i < fileNames.length; i++)
		{
			String fileName = propertyPath + "/" + fileNames[i] + properties;
			convertPropertyFile(fileName, i);
			try
			{
				generateGwtJsonObject(i, fileName);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			// Convert all languages
			convertAllLanguages(i);
		}
	}

	/**
	 * @param i
	 */
	private void convertAllLanguages(int i)
	{
		String fileName;
		for (int j = 0; j < languages.length; j++)
		{
			fileName = propertyPath + "/" + fileNames[i] + "_" + languages[j] + properties;
			convertPropertyFile(fileName, i);
		}
	}

	/**
	 * @param fileName
	 * @param fileNameIndex
	 */
	private void convertPropertyFile(String fileName, int fileNameIndex)
	{
		try
		{
			convertPropertyToJson(fileName, fileNameIndex);
		}
		catch (IOException e)
		{
			System.out.println("Error while processing " + fileName);
			e.printStackTrace();
		}
	}

	public void generateGwtJsonObject(int fileNameIndex, String fileName) throws IOException
	{
		// load property file
		JSONObject propJSON = loadPropertyFile(fileName);

		StringBuffer GWTClassString = new StringBuffer();
		GWTClassString.append(PACKAGE_NAME);
		GWTClassString.append("import java.util.Map;\n");
		GWTClassString.append("import java.util.LinkedHashMap;\n");
		GWTClassString.append("import com.google.gwt.json.client.JSONObject;\n");
		String className = fileNames[fileNameIndex];
		GWTClassString.append("public class " + className);
		GWTClassString.append("Json extends JSONObject implements " + className);
		GWTClassString.append("{ \n");
		String[] names = JSONObject.getNames(propJSON);
		for (int i = 0; i < names.length; i++)
		{
			String currentProperty = names[i];
			if (!currentProperty.contains("Map"))
				addRegularMethod(GWTClassString, className, propJSON, currentProperty);
			else
				addMapMethod(GWTClassString, className, propJSON, currentProperty);
		}
		GWTClassString.append("\n} ");

		// Write generated code to file
		String outputFile = jsonPath + "/" + className + "Json.java";
		PrintWriter fos =
		    new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8));
		fos.append(GWTClassString.toString());
		fos.close();
	}

	/**
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private JSONObject loadPropertyFile(String fileName) throws FileNotFoundException, IOException
	{
		File fh = new File(fileName);
		FileInputStream fis = new FileInputStream(fh);
		Properties pConfig = new Properties();
		pConfig.load(fis);
		JSONObject propJSON = Property.toJSONObject(pConfig);
		return propJSON;
	}

	/**
	 * @param GWTClassString
	 * @param className
	 * @param propJSON
	 * @param currentProperty
	 */
	private void addMapMethod(StringBuffer GWTClassString, String className, JSONObject propJSON, String currentProperty)
	{
		GWTClassString.append("Map<String, String> internal" + currentProperty + " = null;\n");
		GWTClassString.append("public void add" + currentProperty + "Entry(String key, String value) {	internal" +
		    currentProperty + ".put(key, value);}");
		GWTClassString.append("@Override\n");
		GWTClassString.append("public Map<String, String> " + currentProperty + "(){if (internal" + currentProperty +
		    "==null){\n");
		GWTClassString.append("internal" + currentProperty + " = new LinkedHashMap<String, String>();\n");
		GWTClassString.append("String stringValues = get" + currentProperty + "();\n");
		GWTClassString.append("String[] valueArray = stringValues.trim().split(\",\");\n");
		GWTClassString.append("for (int i = 0; i < valueArray.length; i++)\n");
		GWTClassString.append("{add" + currentProperty + "ValueForKey(valueArray[i].trim());}\n");
		GWTClassString.append("}return internal" + currentProperty + ";}\n");
		GWTClassString.append("private final native String get" + currentProperty + "() /*-{return $wnd." + className +
		    "." + currentProperty + ";}-*/;");
		GWTClassString.append("public final native void add" + currentProperty + "ValueForKey(String key) /*-{\n");
		GWTClassString.append("var value = $wnd." + className + "[key];\n");
		GWTClassString.append("this.@com.roksoft.pocoso3.client.resources." + className + "Json::add" +
		    currentProperty + "Entry(Ljava/lang/String;Ljava/lang/String;)(key, value);}-*/;\n");
	}

	/**
	 * @param GWTClassString
	 * @param className
	 * @param propJSON
	 * @param currentProperty
	 */
	private void addRegularMethod(
	    StringBuffer GWTClassString,
	    String className,
	    JSONObject propJSON,
	    String currentProperty)
	{
		GWTClassString.append("public final native String get" + currentProperty + "() /*-{	return $wnd." + className +
		    "." + currentProperty + ";}-*/;\n");
		GWTClassString.append("@Override\n");
		GWTClassString.append("public String " + currentProperty + "(){return get" + currentProperty + "();}\n");
	}

	/**
	 * @param fileName
	 * @param fileNameIndex
	 * @throws IOException
	 */
	public void convertPropertyToJson(String fileName, int fileNameIndex) throws IOException
	{
		// load property file
		JSONObject propJSON = loadPropertyFile(fileName);

		// Convert to JSON
		String outputFile = fileName.toString();
		outputFile = outputFile.replace(propertyPath, jsonPath);
		outputFile = outputFile.replace(properties, json);
		PrintWriter fos =
		    new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8));
		String jsonString = propJSON.toString();
		jsonString = jsonString.replace("'", "\\'");
		fos.print("var " + fileNames[fileNameIndex] + " = JSON.parse('" + jsonString + "');\n");
		fos.close();
	}
}
