# README #


### What is GWTPropertyConverter? ###

This application converts GWT language constants into JSON files and generates the required Java implementation. At the moment it is tightly integrated with another project and needs minor modifications to be used in another context.
All you have to do is:
* Fork it
* Clear the input/output folder
* Rename the hardcoded file names in Main.java or get them passed as parameters or find all .properties files in a folder that is passed as parameter
